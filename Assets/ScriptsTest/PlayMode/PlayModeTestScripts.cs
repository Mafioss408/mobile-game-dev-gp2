using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class PlayModeTestScripts
{

    [UnityTest]
    public IEnumerator PlayModeTestScriptsWithEnumeratorPasses()
    {
        var gameObject = new GameObject();

        var testScripts = gameObject.AddComponent<Test2>();
        testScripts.MoveUp();
        // Use the Assert class to test conditions.
        // Use yield to skip a frame.
        yield return null;


        Assert.AreEqual(new Vector3(0,1,0), gameObject.transform.position);
    }
}
