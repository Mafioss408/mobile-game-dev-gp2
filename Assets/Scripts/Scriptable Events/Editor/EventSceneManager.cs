﻿
using System;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
using UnityEngine;
using System.Linq;


[InitializeOnLoad]
public static class EventSceneManager
{
    // Serve a risettare le reference al cambio della scena


    static EventSceneManager()
    {
        EditorSceneManager.activeSceneChangedInEditMode += FatchEvents;
    }

    private static void FatchEvents(Scene scene, Scene _loadSceneMode)
    {
        var guids = AssetDatabase.FindAssets("t:ScriptableObject", new[] { "Assets/Scripts/SalinbeniTIP/ScriptableEvents/Editor" });

        foreach (var guid in guids)
        {
            var path = AssetDatabase.GUIDToAssetPath(guid);

            var _event = AssetDatabase.LoadAssetAtPath<ScriptableEvent>(path);

            if (_event)
            {
                Debug.Log($"Found{_event.name}!");

                _event.Clear();

                var items = GameObject.FindObjectsOfType<EventListener>().Where(listener => listener.Event == _event);

                foreach (var listener in items)
                {
                    _event.Add(listener);
                }
            }

        }
    }
}
