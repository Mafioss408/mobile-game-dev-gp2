using UnityEngine;
using UnityEngine.Advertisements;


public class ADSManager : MonoBehaviour, IUnityAdsInitializationListener, IUnityAdsShowListener, IUnityAdsLoadListener
{
#if UNITY_IOS


string  gameId = "5474987";
 

#else

    readonly string gameId = "5474986";

#endif

    private bool testMode = true;

    readonly string androidIntID = "Interstitial_Android";
    readonly string androidRewID = "Rewarded_Android";
    readonly string androidBanID = "Banner_Android";

    [SerializeField] private BannerPosition _position = BannerPosition.CENTER;




    #region Inistializzazione 

    private void Start()
    {
        Advertisement.Initialize(gameId, testMode, this);

    }

    public void OnInitializationComplete()
    {
        Debug.Log("Inizializzazione completata");
    }

    public void OnInitializationFailed(UnityAdsInitializationError error, string message)
    {
        Debug.Log($"Inizialization fail: {error.ToString()} - {message}");

    }
    #endregion

    #region Intersitial Show
    public void LoadAdIntersitial()
    {
        Debug.Log("Loading Ad " + androidIntID);
        Advertisement.Load(androidIntID, this);
        
    }

    public void ShowAdIntersitial()
    {
        Advertisement.Show(androidIntID, this);
    }
    #endregion

    #region Reward Show
    public void LoadAdReward()
    {
        Debug.Log("Loading Ad " + androidIntID);
        Advertisement.Load(androidRewID, this);
    }

    public void ShowAdReward()
    {
        Advertisement.Show(androidRewID, this);
    }
    #endregion
    public void OnUnityAdsShowFailure(string placementId, UnityAdsShowError error, string message)
    {

    }

    public void OnUnityAdsShowStart(string placementId)
    {

    }

    public void OnUnityAdsShowClick(string placementId)
    {

    }

    public void OnUnityAdsShowComplete(string placementId, UnityAdsShowCompletionState showCompletionState)
    {


    }

    public void OnUnityAdsAdLoaded(string placementId)
    {
        if (placementId.Equals(androidIntID))
            ShowAdIntersitial();
        if (placementId.Equals(androidRewID))
            ShowAdReward();

    }

    public void OnUnityAdsFailedToLoad(string placementId, UnityAdsLoadError error, string message)
    {

    }
}
