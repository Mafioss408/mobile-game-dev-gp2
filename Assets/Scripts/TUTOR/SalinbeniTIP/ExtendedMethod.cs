using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtendedMethod
{
    // per chiamare extension metods la classe deve essere statica
    // il this nel metodo attiva il tutto
    // il ? rende una variabile nullabile

    public static Vector3 Add(this Vector3 position, float? x = null, float? y = null, float? z = null)
    {
        // ?? vengono chiamati null-coalescing
        return position + new Vector3(x ?? 0, y ?? 0, z ?? 0);
    }

    // rigidbody2D ??= GetComponent<Rigidbody2D>();  --> se � nullo va a fare il get seno la ignora
}
