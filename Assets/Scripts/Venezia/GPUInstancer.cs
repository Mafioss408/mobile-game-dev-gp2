using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GPUInstancer : MonoBehaviour
{

    public int batchCount;
    public int batchLenght;

    public int meshScale = 1;


    public GameObject prefab;
    public Mesh mesh;
    public Material[] materials;



    private List<List<Matrix4x4>> matrices = new List<List<Matrix4x4>>();
    private List<Transform> objToMove = new List<Transform>();

    private void Start()
    {
        //  Matrix4x4.TRS(transform.position, Quaternion.identity, Vector3.one);

        for (int i = 0; i < batchCount; i++)
        {
            matrices.Add(new List<Matrix4x4>());

            for (int j = 0; j < batchLenght; j++)
            {
                // Vector3 randomVec = new Vector3(Random.Range(-50f, 50f), Random.Range(-50f, 50f), Random.Range(-50f, 50f));
                Vector3 randomVec = Random.insideUnitSphere * Random.Range(-50f, 50f);
                matrices[i].Add(Matrix4x4.TRS(randomVec, Quaternion.identity, Vector3.one * meshScale));
            }
        }

        //for (int i = 0; i < 5000; i++)
        //{
        //    Vector3 randomVec = Random.insideUnitSphere * Random.Range(-50f, 50f);
        //    objToMove.Add(Instantiate(prefab, randomVec, Quaternion.identity).transform);
        //}

    }

    private void Update()
    {
        RenderMeshes();
        //MoveObj();
    }

    private void MoveObj()
    {
        for (int i = 0; i < objToMove.Count; i++)
        {
            Vector3 pos = objToMove[i].position;
            pos.y = Mathf.Sin(Mathf.PI * (pos.x + Time.time));
            objToMove[i].position = pos;
        }
    }

    private void RenderMeshes()
    {
        foreach (var matrix in matrices)
        {
            for (int i = 0; i < matrix.Count; i++)
            {
                Vector3 pos = matrix[i].GetPosition();
                pos.y = Mathf.Sin(Mathf.PI * (pos.x + Time.time));
                matrix[i] = Matrix4x4.TRS(pos, Quaternion.identity, Vector3.one * meshScale);
            }
            for (int i = 0; i < mesh.subMeshCount; i++)
            {
                Graphics.DrawMeshInstanced(mesh, i, materials[i], matrix);
            }
        }
    }
}
