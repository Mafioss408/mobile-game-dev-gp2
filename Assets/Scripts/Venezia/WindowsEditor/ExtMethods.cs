using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtMethods
{
    public static Vector3 Round(this Vector3 value)
    {
        value.x = Mathf.Round(value.x);
        value.y = Mathf.Round(value.y);
        value.z = Mathf.Round(value.z);
        return value;
    }

    public static Vector3 Round(this Vector3 v, float size)
    {
        return (v / size).Round() * size;
    }

    public static float Round(this float v, float size)
    {
        return Mathf.Round(v / size);
    }
}
