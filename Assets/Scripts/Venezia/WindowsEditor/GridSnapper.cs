using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public static class GridSnapper
{

    [MenuItem("WindowsTools/Snapper %&S", isValidateFunction: true)]
    public static bool SnapValidation()
    {
        return Selection.gameObjects.Length > 0;
    }

    [MenuItem("WindowsTools/Snapper %&S")]
    public static void Snap()
    {
        foreach (GameObject item in Selection.gameObjects)
        {
            // registrare solo quello che serve - in questo caso solo il transform
            Undo.RecordObject(item.transform, "Snap stuff");
            Vector3 pos = item.transform.position.Round();
            item.transform.position = pos;
        }
    }
}
