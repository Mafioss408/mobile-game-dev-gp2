using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Spline))]
public class Spline_Editor : Editor
{
    private static Color[] modeColors =
    {
        Color.white,
        Color.yellow,
        Color.cyan
    };

    private const int curveResolution = 10;
    private const float directionScale = 0.5f;
    private const int stepsCurve = 10;
    private const float handleSize = 0.05f;
    private const float pickSize = 0.08f;

    private int selectedIndex = -1;

    private Spline spline;
    Transform handleTransform;
    Quaternion handleRotation;

    public override void OnInspectorGUI()
    {
        spline = target as Spline;

        EditorGUI.BeginChangeCheck();
        bool loop = EditorGUILayout.Toggle("Loop", spline.Loop);
        if (EditorGUI.EndChangeCheck())
        {
            spline.Loop = loop;
            Undo.RecordObject(spline, "Toggle loop");
            SceneView.RepaintAll();
        }


        if (selectedIndex >= 0 && selectedIndex < spline.ControlPointCount)
        {
            DrawPointInInspector();
        }
        if (GUILayout.Button("Add Curve"))
        {
            Undo.RecordObject(spline, "Add Curve");
            spline.AddCurve();
            // EditorUtility.SetDirty(spline);
        }
    }

    private void OnSceneGUI()
    {

        spline = target as Spline;

        handleTransform = spline.transform;
        handleRotation = Tools.pivotRotation == PivotRotation.Local ? handleTransform.rotation : Quaternion.identity;

        Vector3 p0 = DrawPoint(0);

        for (int i = 1; i < spline.ControlPointCount; i += 3)
        {
            Vector3 p1 = DrawPoint(i);
            Vector3 p2 = DrawPoint(i + 1);
            Vector3 p3 = DrawPoint(i + 2);

            Handles.color = Color.gray;
            Handles.DrawAAPolyLine(p0, p1);
            Handles.DrawAAPolyLine(p2, p3);

            Handles.DrawBezier(p0, p3, p1, p2, Color.green, null, 2f);
            p0 = p3;
        }


        ShowVelocity();

    }

    private Vector3 DrawPoint(int index)
    {
        Vector3 point = handleTransform.TransformPoint(spline.GetControlPoint(index));

        float size = HandleUtility.GetHandleSize(point);

        if(index == 0)
        {
            size *= 2;
        }


        Handles.color = modeColors[(int)spline.GetControlPointMode(index)];

        if (Handles.Button(point, handleRotation, handleSize * size, pickSize * size, Handles.DotHandleCap))
        {
            selectedIndex = index;
            Repaint();  // -- serve ad aggiornare la ui ogni volta
        }
        if (selectedIndex == index)
        {
            EditorGUI.BeginChangeCheck();
            point = Handles.DoPositionHandle(point, handleRotation);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(spline, "Point p0 moved");
                EditorUtility.SetDirty(spline);
                spline.SetControlPoint(index, handleTransform.InverseTransformPoint(point));
            }
        }
        return point;
    }

    private void ShowVelocity()
    {
        #region Versione non compattata
        //Handles.color = Color.green;
        //Vector3 lineStart = curve.GetPoint(0);
        //Handles.color = Color.magenta;
        //Handles.DrawAAPolyLine(lineStart, lineStart + curve.GetDirection(0));


        //for (int i = 1; i <= curveResolution; i++)
        //{
        //    Vector3 lineEnd = curve.GetPoint(i / (float)curveResolution);
        //    Handles.color = Color.green;
        //    Handles.DrawAAPolyLine(lineStart, lineEnd);
        //    Handles.color = Color.magenta;
        //    Handles.DrawAAPolyLine(lineEnd, lineEnd + curve.GetDirection(i / (float)curveResolution));
        //    lineStart = lineEnd;
        //    //i / resolution
        //}
        #endregion

        Handles.color = Color.magenta;

        int steps = stepsCurve * spline.CurveCount;


        for (int i = 0; i <= steps; i++)
        {
            Vector3 point = spline.GetPoint(i / (float)steps);
            Handles.DrawAAPolyLine(point, point + spline.GetDirection(i / (float)steps) * directionScale);
        }
    }

    private void DrawPointInInspector()
    {
        GUILayout.Label("Point");
        EditorGUI.BeginChangeCheck();
        Vector3 point = EditorGUILayout.Vector3Field("Position", spline.GetControlPoint(selectedIndex));
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(spline, "Move Point");
            spline.SetControlPoint(selectedIndex, point);
        }

        EditorGUI.BeginChangeCheck();
        CurveControlPointMode mode = (CurveControlPointMode)EditorGUILayout.EnumPopup("Mode", spline.GetControlPointMode(selectedIndex));
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(spline, "Change Point Mode");
            spline.SetControlPointMode(selectedIndex, mode);
        }
    }
}

