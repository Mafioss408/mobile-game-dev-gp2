using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;



[CustomEditor(typeof(Trap))]
public class Trap_Editor : Editor
{
    #region TEST VAR
    //public enum myEnum
    //{
    //    Uno,
    //    Due,
    //    Tre
    //}

    //private myEnum _myEnum;
    //float fl;
    // da tenere in mente -- serializzazione
    #endregion

    SerializedObject so;
    SerializedProperty propRadius;
    SerializedProperty propDamage;
    SerializedProperty propColor;


    private void OnEnable()
    {
        so = new SerializedObject(target);
        //so = serializedObject; // shorthand


        propRadius = so.FindProperty("radius");
        propDamage = so.FindProperty("damage");
       // propColor = so.FindProperty("ovverrideColor");


    }

    // per disegnare GUI, GUILayout => funzioni da poter utilizzare in inspector o editor
    //
    // EditorGUILayout ,EditorGUI,
    public override void OnInspectorGUI()
    {
        #region TEST
        //// label sono delle etichette
        ////Rect r = new Rect(Vector2.one, Vector2.one * 100);
        //// GUI.Label(r, "TestGuI");
        //GUILayout.BeginHorizontal();
        //GUILayout.Label("TestLabel");

        //if (GUILayout.Button("MyButton"))
        //{
        //    Debug.Log("Pressed");
        //}
        //GUILayout.EndHorizontal();

        //GUILayout.BeginHorizontal();
        //_myEnum = (myEnum)EditorGUILayout.EnumPopup(_myEnum, GUILayout.Width(100f));
        //fl = GUILayout.HorizontalSlider(fl, -100f, 100f);
        //GUILayout.EndHorizontal();


        //GUILayout.BeginVertical();
        //GUILayout.Label("TestLabel");

        //if (GUILayout.Button("MyButton"))
        //{
        //    Debug.Log("Pressed");
        //}
        //GUILayout.EndVertical();



        //using (new GUILayout.HorizontalScope(EditorStyles.helpBox))
        //{
        //    GUILayout.Label("TestLabel");

        //    if (GUILayout.Button("MyButton"))
        //    {
        //        Debug.Log("Pressed");
        //    }
        //}

        //GUILayout.Space(40);
        //EditorGUILayout.ObjectField("Transform", null, typeof(Transform), true);
        //GUILayout.Space(50);
        //using (new GUILayout.VerticalScope(EditorStyles.helpBox))
        //{
        //    GUILayout.Label("TestLabel0", EditorStyles.toolbar);
        //    GUILayout.Label("TestLabel1", EditorStyles.toolbarButton);
        //    GUILayout.Label("TestLabel2", EditorStyles.iconButton);
        //    GUILayout.Label("TestLabel3", EditorStyles.boldLabel);
        //    GUILayout.Label("TestLabel4",EditorStyles.foldout);

        //}
        #endregion

        #region PRIMO METODO CUSTUM INSPECTOR
        //Trap trap = target as Trap;

        //float newRadius = EditorGUILayout.FloatField("Radius", trap.radius);
        //if(newRadius != trap.radius)
        //{
        //    Undo.RecordObject(trap, "TrapChanged");
        //    trap.radius = newRadius;
        //}

        //trap.damage = EditorGUILayout.IntField("Damage", trap.damage);
        #endregion


        #region SECONDO METODO CUSTUM INSPECTOR
        so.Update();
        EditorGUILayout.PropertyField(propRadius);
        EditorGUILayout.PropertyField(propDamage);
        //EditorGUILayout.PropertyField(propColor);

        if (so.ApplyModifiedProperties())
        {
            Trap.OverrideColors();
        }
        #endregion
    }

    //private void DrawMyStructure()
    //{
    //    using (new GUILayout.HorizontalScope())
    //    {

    //    }                                                                                                                                                                                                                                    
    //}
}