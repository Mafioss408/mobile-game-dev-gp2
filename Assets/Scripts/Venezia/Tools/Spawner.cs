using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[ExecuteAlways] // pericoloso!
public class Spawner : MonoBehaviour
{
    public Color element = Color.cyan;





    static readonly int propCol = Shader.PropertyToID("_Color");

    MaterialPropertyBlock mpb;
    public MaterialPropertyBlock Mpd
    {
        get
        {
            if (mpb == null)
            {
                mpb = new MaterialPropertyBlock();
            }
            return mpb;

        }
    }  //  serve essensialmente ad evitare istanze del material   -- non si pu� inizializzare direttamente --  o onEn o get 

    private void OnValidate()
    {
        ApplyColor();
    }

    private void OnEnable()
    {
        // GetComponent<MeshRenderer>().material.color = element;
        //GetComponent<MeshRenderer>().sharedMaterial.color = element;  attenzione al memory leak
        //GetComponent<MeshFilter>().sharedMesh

        //Shader shader = Shader.Find("Deafult/Diffuse");
        //Material mat = new Material(shader);
        //mat.hideFlags = HideFlags.HideAndDontSave;  // per evitare memory leak 

        Trap.spawners.Add(this);
    }

    private void OnDisable()
    {
        Trap.spawners.Remove(this);
    }

    public void ApplyColor()
    {
        MeshRenderer rnd = GetComponent<MeshRenderer>();

         
        //rnd.material.color = element;
        //rnd.material.SetColor(propCol, element);


        Mpd.SetColor(propCol, element);
        rnd.SetPropertyBlock(Mpd);  // --- stare attenti a ci� che si passa al set property
        
    }
    public void ApplyColor(Color col)
    {
        MeshRenderer rnd = GetComponent<MeshRenderer>();


        //rnd.material.color = element;
        //rnd.material.SetColor(propCol, element);


        Mpd.SetColor(propCol, element);
        rnd.SetPropertyBlock(Mpd);
    }
}
