using System.Collections;
using System.Collections.Generic;
using UnityEngine;


#if UNITY_EDITOR
using UnityEditor;
#endif



public class Trap : MonoBehaviour
{
    public static Color ovverrideColor;
    public int damage = 10;
    [Min(0.5f)] public float radius;
 

    public static List<Spawner> spawners = new List<Spawner>();

    public TestChildClass testClass = new TestChildClass();

    public static void OverrideColors()
    {
        for (int i = 0; i < spawners.Count; i++)
        {
            spawners[i].ApplyColor(ovverrideColor);
        }
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Handles.zTest = UnityEngine.Rendering.CompareFunction.LessEqual;
  
        foreach (var spawner in spawners)
        {
             Gizmos.DrawLine(transform.position, spawner.transform.position);
            //Handles.DrawAAPolyLine(transform.position,spawner.transform.position);

            Vector3 trapPos = transform.position;
            Vector3 spawnerPos = spawner.transform.position;
            float halfPoint = (trapPos.y - spawnerPos.y) * 0.5f;
            Vector3 offset = Vector3.up * halfPoint;

            Handles.DrawBezier(trapPos, spawnerPos, trapPos - offset, spawnerPos + offset, Color.green, EditorGUIUtility.whiteTexture, 1f);
        }


        //Gizmos.color = element;
        //Gizmos.DrawWireSphere(transform.position, radius);
    }
    private void OnDrawGizmosSelected()
    {

    }

#endif

}


[System.Serializable]
public class TestClass
{
    public Vector3 pos;
    public Color col;
}

[System.Serializable]
public class TestChildClass : TestClass
{
    public int i;
}
