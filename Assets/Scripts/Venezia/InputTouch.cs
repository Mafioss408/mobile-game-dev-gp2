using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public enum DirectionType
{
    Still,
    Right,
    Left,
    Up,
    Down

}
public class InputTouch : MonoBehaviour
{

    [SerializeField] private TextMeshProUGUI phaseText;
    [SerializeField] private GameObject itemToMove;

    [SerializeField] private float speedObject;

    public float dpadSens = 0.2f;
    private DirectionType directionType = DirectionType.Still;

    private Vector2 touchStartPosition, touchEndPosition;
    void Start()
    {

    }


    void Update()
    {
        if (Input.touchCount <= 0)
        {
            phaseText.text = " No touch ";
            return;
        }

        Touch touch = Input.GetTouch(0);

        //string touchPhase = touch.phase.ToString();
        //phaseText.text = touchPhase;
        //// attenzione alla camera.. questo metodo funziona in ortografico;
        ////Vector3 touchPosition = Camera.main.ScreenToWorldPoint(new Vector3(touch.position.x,touch.position.y,10f));
        //Vector3 touchPosition = Camera.main.ScreenToWorldPoint(touch.position);
        //touchPosition.z = 0;
        //itemToMove.transform.position = Vector3.MoveTowards(itemToMove.transform.position,touchPosition,speedObject * Time.deltaTime);

        if (touch.phase == TouchPhase.Began)
        {
            touchStartPosition = touch.position;
        }
        else if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Ended)
        {
            touchEndPosition = touch.position;

            float x = touchEndPosition.x - touchStartPosition.x;
            float y = touchEndPosition.y - touchStartPosition.y;

            if (Mathf.Abs(x) <= dpadSens && Mathf.Abs(y) <= dpadSens)
            {
                directionType = DirectionType.Still;
            }
            else if (Mathf.Abs(x) > Mathf.Abs(y))
            {
                directionType = x > 0 ? DirectionType.Right : DirectionType.Left;
            }
            else
            {
                directionType = y > 0 ? DirectionType.Up : DirectionType.Down;
            }
        }

        phaseText.text = directionType.ToString();
        MoveCube();

        foreach (var tmp in Input.touches)
        {
            Vector3 tmp1 = Camera.main.ScreenToWorldPoint(tmp.position);
            tmp1.z = 0f;
            Debug.DrawRay(itemToMove.transform.position, tmp1, Color.red);

        }

        //for (int i = 0; i < Input.touches.Length; i++)
        //{
        //    Vector3 tmp = Camera.main.ScreenToWorldPoint(Input.touches[i].position);
        //    tmp.z = 0f;
        //    Debug.Log(tmp);
        //    Debug.DrawRay(itemToMove.transform.position, tmp, Color.red);
        //}

    }

    private void MoveCube()
    {
        Vector2 moveDir = Vector2.zero;

        switch (directionType)
        {
            case DirectionType.Still:
                moveDir = Vector2.zero;
                break;
            case DirectionType.Right:
                moveDir = Vector2.right;
                break;
            case DirectionType.Left:
                moveDir = Vector2.left;
                break;
            case DirectionType.Up:
                moveDir = Vector2.up;
                break;
            case DirectionType.Down:
                moveDir = Vector2.down;
                break;
            default:
                break;
        }

        itemToMove.transform.Translate(moveDir * speedObject * Time.deltaTime);
    }
}
