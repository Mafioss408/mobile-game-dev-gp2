using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GyroController : MonoBehaviour
{


    [SerializeField] private float speed;
    [SerializeField] private float smoothing;

    [Header("UI")]
    [SerializeField] TextMeshProUGUI attitude_tx;
    [SerializeField] TextMeshProUGUI rotation_tx;
    [SerializeField] TextMeshProUGUI offsetRotation_tx;
    [SerializeField] TextMeshProUGUI rawRotation_tx;


    private Transform rawGryroRotation;
    private Quaternion gyroInitialRotation;
    private Quaternion offsetRotation;
    private Quaternion initialRotation;


    GameObject cameraRig;

    private bool gyroEnabled;
    private Gyroscope gyro;
    Quaternion rot = new Quaternion(0, 0, 1, 0);

    private void Start()
    {


        gyroEnabled = GyroEnabled();

        cameraRig = new GameObject("Camera Rig");
        cameraRig.transform.position = Camera.main.transform.position;
        Camera.main.transform.SetParent(cameraRig.transform);
        //cameraRig.transform.rotation = Quaternion.Euler(90,90,0);  
        rawGryroRotation = cameraRig.transform;

        initialRotation = cameraRig.transform.rotation;
        Recalibrate();

        rawGryroRotation.position = transform.position;
        rawGryroRotation.rotation = transform.rotation;
    }

    private void Update()
    {

        if (gyroEnabled)
        {
            ApplyGyroRotation();
            cameraRig.transform.rotation = Quaternion.Slerp(cameraRig.transform.rotation, initialRotation * rawGryroRotation.rotation, smoothing);
            UpdateUI();
        }//transform.rotation = gyro.attitude;

    }

    private void ApplyGyroRotation()
    {
        offsetRotation = Quaternion.Inverse(gyroInitialRotation * GyroToWorld(Input.gyro.attitude));
        float currentSpeed = Time.deltaTime * speed;

        Quaternion tempRot = new Quaternion(
            offsetRotation.x * currentSpeed,
            offsetRotation.y * currentSpeed,
            0f,
            offsetRotation.w * currentSpeed);

        rawGryroRotation.rotation = tempRot;
    }

    private bool GyroEnabled()
    {
        if (!SystemInfo.supportsGyroscope) return false;

        gyro = Input.gyro;
        gyro.enabled = true;
        return true;
    }

    public void Recalibrate()
    {
        gyroInitialRotation =
            GyroToWorld(Input.gyro.attitude);
    }

    private Quaternion GyroToWorld(Quaternion gyroQuat)
    {
        return new Quaternion(Input.gyro.attitude.x, Input.gyro.attitude.y, -Input.gyro.attitude.z, -Input.gyro.attitude.w);
    }

    private void UpdateUI()
    {
        attitude_tx.text = "Attitude: " + Input.gyro.attitude.ToString();
        //string str = $"Attitude:{Input.gyro.attitude}";
        rotation_tx.text = "Rotation: " + transform.rotation.ToString();
        offsetRotation_tx.text = "Offset Rotation: " + offsetRotation.ToString();
        rawRotation_tx.text = "rawRotation: " + rawGryroRotation.rotation.ToString();
    }


}
