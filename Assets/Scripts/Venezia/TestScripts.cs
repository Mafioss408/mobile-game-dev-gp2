using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Events;

public class TestScripts : MonoBehaviour
{

    public Button btn;
  //  public delegate void MyDelegate();
    public event EventHandler onMyDelegate;
    EventHandler handler;

    public UnityEvent MyEvent;

    // Start is called before the first frame update
    void Start()
    {

        // funzione anonima
         //handler = (sender, e) => { Debug.Log("Message");};

        onMyDelegate += handler;

        //btn.onClick.AddListener(() => { Debug.Log("Message"); });
        MyEvent.AddListener(LogMessage);
        MyEvent.AddListener(LogMessage2);
        //  Screen.orientation = ScreenOrientation.Portrait;

        btn.onClick.AddListener(LogMessage);
        btn.onClick.AddListener(LogMessage2);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
           // onMyDelegate(this, null);
           MyEvent?.Invoke();
        }
        else if(Input.GetKeyDown(KeyCode.A))
        {
            onMyDelegate -= handler;
        }
    }


    private void LogMessage()
    {
        Debug.Log("Message2");
    }
    private void LogMessage2()
    {
        Debug.Log("Message3");
    }
}
