using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Profiling;

public class LinQTest : MonoBehaviour
{

    public List<GameObject> myGameObjects = new List<GameObject>();
    public List<GameObject> myGameObjects1 = new List<GameObject>();


    CustomSampler sampler;
    CustomSampler sampler2;

    private void Start()
    {

        sampler = CustomSampler.Create("LinqNo");
        sampler2 = CustomSampler.Create("Linq");
    }
    private void Update()
    {


        List<GameObject> tempList1 = myGameObjects;
        List<GameObject> tempList2 = myGameObjects1;

        StartCoroutine(GetNearesGameObjectLINQ(tempList2));
        //GetNearesGameObject(tempList1);
        //sampler.End();

        //sampler2.Begin();
        //GetNearesGameObjectLINQ(tempList2);
        //sampler2.End();
    }

    private GameObject GetNearesGameObject(List<GameObject> objects)
    {
        float smallestDist = Mathf.Infinity;

        GameObject nearest = null;

        foreach (var obj in objects)
        {
            float distance = Vector3.Distance(transform.position, obj.transform.position);
            if (distance < smallestDist)
            {
                smallestDist = distance;
                nearest = obj;
            }
        }
        return nearest;
    }


    //private GameObject GetNearesGameObjectLINQ(List<GameObject> objects)
    //{
    //    objects.OrderBy(t => Vector3.Distance(transform.position, t.transform.position)).ThenBy(t => t.transform.localScale);
    //    var nearest = objects.FirstOrDefault();
    //    var stuff = objects.Take(3);
    //    return nearest;
    //}
    private IEnumerator GetNearesGameObjectLINQ(List<GameObject> objects)
    {
        var nearest = objects.OrderBy(t => Vector3.Distance(transform.position, t.transform.position)).ThenBy(t => t.transform.localScale).FirstOrDefault();

        yield return new WaitForSeconds(1);

        if (Input.GetKeyDown(KeyCode.Space)) // avviene qua
        {
            foreach (var obj in objects)
            {
                obj.transform.localScale = Vector3.one * 3;
            }
        }
        else
        {
            Debug.Log(nearest);
        }

    }

}
