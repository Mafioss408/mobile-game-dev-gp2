using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Accelerometer : MonoBehaviour
{
    /// <summary>
    /// Quando si lavora con l'acceleromentro tenere in considerazione 
    /// il fattore del sampli che si puo decidere se filtrarlo o bypassarlo
    /// 
    /// </summary>
    float accelerometerUpdateInterval = 1.0f / 60.0f;  // frequenza accelerometro di base
    public float lowPassKernelWithInSeconds = 1.0f; // valore che si utilizza per manipolare la frequenza
    float lowPassFilterFactor;


    public float forceMultiplier = 5;
    Rigidbody rb;

    private Vector3 lowPassValue = Vector3.zero;

    void Start()
    {
        rb = GetComponent<Rigidbody>();

        lowPassFilterFactor = accelerometerUpdateInterval / lowPassKernelWithInSeconds;
        lowPassValue = Input.acceleration;

        Screen.orientation = ScreenOrientation.LandscapeLeft;
   
    }


    private void Update()
    {
        lowPassValue = LowPassFilterAccelerometer(lowPassValue);
    }
    private void FixedUpdate()
    {

        Vector3 tilt = lowPassValue;
        tilt = Quaternion.Euler(90, 0, 0) * tilt;

        if (tilt.magnitude <= 1f) return;

        rb.AddForce(tilt * forceMultiplier);
        Debug.DrawRay(transform.position + Vector3.up, tilt, Color.magenta);

    }

    Vector3 GetAccelerometerValue()
    {
        Vector3 acc = Vector3.zero;
        float period = 0.0f;

        foreach (AccelerationEvent events in Input.accelerationEvents)
        {
            acc += events.acceleration * events.deltaTime;
            period += events.deltaTime;
        }

        if (period > 0.0f)
        {
            acc *= 1.0f / period;
        }

        return acc;
    }

    Vector3 LowPassFilterAccelerometer(Vector3 prevValue)
    {
        Vector3 newValue = Vector3.Lerp(prevValue, Input.acceleration, lowPassFilterFactor);
        return newValue;
    }
}
