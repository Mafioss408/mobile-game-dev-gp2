using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.Profiling;
using UnityEngine.UI;

public class Test2 : MonoBehaviour
{

    TouchScreenKeyboard keybord;

    CustomSampler sampler;
    CustomSampler sampler2;
    MathStuff ms;

    public Button btn;

    /// <summary>
    /// La tastiera esce fuori unicamente in build
    /// Quindi per test fare build and run
    /// </summary>



    void Start()
    {

        sampler = CustomSampler.Create("Test sempler");
        sampler2 = CustomSampler.Create("Test sempler2");

        ms = new MathStuff();

        Debug.Log($"Tastiera supportata {TouchScreenKeyboard.isSupported}");
        keybord = TouchScreenKeyboard.Open("", TouchScreenKeyboardType.Default, false, false, true, false);


        //// best practis inserire all'interno del gioco un modo per attivarli in caso l'utente dovesse negarli
        //if (!Permission.HasUserAuthorizedPermission(Permission.FineLocation))
        //{
        //    Permission.RequestUserPermission(Permission.FineLocation);
        //}
        //else
        //{

        //}

        //btn.onClick.AddListener(MoveUp);
       // btn.onClick.AddListener(() => MoveDown(1));


        // Accedi ai dati del GPS ( servono i permessi )
        //Input.location
    }

    void LambdaFunc()
    {
        MoveDown(1);
    }


    void Update()
    {
        // compilare solo alcune zone di codice

        //  Profiler.BeginSample("My Log");

        sampler.Begin();


        Debug.Log(keybord.active);
        ms.normalDivisionByTwo(10);

        sampler.End();


        sampler2.Begin();


        Debug.Log(keybord.active);
        ms.BetterDivisionByTwo(10);

        sampler2.End();


        if (Input.GetKeyDown(KeyCode.I))
        {
            btn.onClick.AddListener(() => MoveDown(1));
        }
        else if (Input.GetKeyDown(KeyCode.O))
        {
            btn.onClick.RemoveListener(() => MoveDown(1));
        }
        else if (Input.GetKeyDown(KeyCode.P))
        {
            btn.onClick?.Invoke();
        }


        // Profiler.EndSample();

        //if (Input.GetButtonDown("Jump"))
        //{
        //    MoveUp();
        //}
    }

    public void MoveUp()
    {
        transform.position += Vector3.up;
    }
    public void MoveDown(float i)
    {
        print("move down");
    }

}


public class MathStuff
{
    public float normalDivisionByTwo(float value)
    {
        for (int i = 0; i < 10000; i++)
        {
            float c = value;
            c /= 2f;
        }


        return value /= 2f;
    }

    public float BetterDivisionByTwo(float value)
    {
        for (int i = 0; i < 10000; i++)
        {
            float c = value;
            c *= 0.5f;
        }




        return value *= 0.5f;
    }
}


